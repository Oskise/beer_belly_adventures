# Welcome to Beer Belly Adventures!!! Are you ready to embark on an adventure unlike anything you've ever seen before?
# Version: Alpha 0.09

T = 1

import random
import time

# Lists
drunk_level_points = [1, 10, 25, 50, 100, 205, 415, 806, 1487, 2608,
					  4369, 7030, 10922, 16459, 24151, 34618, 48606, 67005, 90869, 121438,
					  160162, 208727, 269083, 343474, 434470, 99999999999
					  ]

drink_rank_points = [0, 0.1, 10, 20, 50, 100, 200, 400, 99999999999]

# Tiers
tiers = {0: "Tasteless",
		 1: "\033[1;32mBitterbrew\033[1;m",
		 2: "\033[1;36mSweetspirit\033[1;m",
		 3: "\033[1;31mFruitflavour\033[1;m",
		 4: "\033[1;33mDivine Distilling\033[1;m"
		 }

# Decorations
dec = {0: "Glittering Stick",
	   1: "Straw",
	   2: "Parasol",
	   3: "Ice Cube",
	   4: "Lemon",
	   5: "Lime",
	   6: "none"
	   }
			   
# Peanut Flavours

fla = {0: ["Plain", "o"],
	   1: ["Salted", "\033[1;37m�\033[1;m"],
	   2: ["Chili", "\033[1;31mo\033[1;m"]
	   }
	 
# Nationalities
nat = {0: ["Swedish", "V�lf�rd", "Life Insurance gives 400% more money"],
	   1: ["German", "Internal Brewing", "Alcohol% multiplier +50%"],
	   2: ["British", "Smart Investor", 'Adds Lottery Price "Increasing Income"'],
	   3: ["Italian", "Economic Crisis", "All shop items are 20% cheaper"],
	   4: ["Spanish", "Gas-Inducing Diet", "Farts give 300% more money"],
	   5: ["American", "Gambler's Instinct", "You're 5 times more likely to win the lottery"]
	   }

# Prices
pilsner_price = [113, 1133, 3311, 9139, 19313]
cider_price = [400, 2000, 4800, 18400, 48200]
wine_price = [911, 7111, 19111, 49111, 149111]
liqueur_price = [1707, 17707, 44770, 117117, 317713]
martini_price = [4555, 45555, 145555, 345555, 745555]

# Introduction to the game
def story(var):
	print("\033[1;31mBeer Belly Adventures Alpha\033[1;m\nVersion: 0.09")
	print("\033[1;32mMade by Oskise\033[1;m")
	time.sleep(T)
	print('Fredde is at the bar again drinking away his life.')
	print('Just press/hold Enter to drink.')
	print('How drunk can you get?')
	print('To see other essential commands, type "help".')
	time.sleep(T * 2)
	# Unlock Achievement "The Brewginning"
	unlock_ach(0, var)

# This is where you choose what to do
def choose_action(var):
	while True:
		print('What will you do:')
		action = input("> ").lower()
		confirm_action(action, var)

# This performs an action depeding on what you do
def confirm_action(action, var):
	commands = {'help': command_help,
				'': drink,
				'profile': profile,
				'fart': fart,
				'shop': shop,
				'ach': achievements,
				'upgrades': upgrades,
				'ib': ib
				}
	try:
		commands[action](var)
	except KeyError as e:
		print("\033[1;30mInvalid action!\033[1;m")
		print(e)

def ib(var):
	# Unlock Achievement "Thrown out the fucking window"
	unlock_ach(10, var)

# For losers who need help		
def command_help(var):
	print('Press/Hold Enter to drink.')
	print('Type "profile" to see stats and other info.')
	print('Type "fart" to fart out all your drunk points and get money.')
	print('Type "shop" to access the shop and buy upgrades.')
	print('Type "ach" to see your achievements.')
	print('Type "upgrades" to see your purchased upgrades.')
	# Unlock Achievement "Read the manual first"
	unlock_ach(1, var)

# The core gameplay. Adds Drunk Points when you drink	
def drink(var):
	points_per_drink = calc_drunk_points(var, True)
	if can_drink(var) == True:
		# Drinking
		var["drinks_had"] += 1
		var["total_drinks_had"] += 1
		var["total_drunk_points"] += points_per_drink
		var["drunk_points"] += points_per_drink
		var["total_drunk_points"] = round(var["total_drunk_points"], 2)
		var["drunk_points"] = round(var["drunk_points"], 2)
		check_lottery(var)
		cap_drunk_points(var)
		print("\033[1;32mDrinks had: " + str(var["drinks_had"]) + "\nDrunk points: " + str(var["drunk_points"]) + "\033[1;m")
		if "Bag of Peanuts" in var["purchased_upgrades"]:
			print(fla[var["nut_level"]][1] + " x " + str(len(var["unl_ach"])))
		# Checks if your Drunk Level has increased
		var["drunk_level"] = drunk_level_up(var)
		check_ach(var, "drink")

def can_drink(var):
	# If your drink doesn't contain alcohol
	if var["alcohol_level"] == 0:
		print("You stare at the glass of water in front of you...")
		print("There's no point in drinking it if it doesn't contain alcohol.")
		print("Go to the shop!")
		return False
	# If you're dead and can't cheat death
	elif var["drunk_points"] >= 434470:
		print("You're dead, you can't drink when you're dead...")
		print("Do I have to spell everything out for you?")
		return False
	else:
		return True

# Checks if you win the lottery
def check_lottery(var):
		if can_gamble(var) == True:
			var["prize_duration"], var["current_prize"] = lottery(var)
			
def cap_drunk_points(var):
	# Caps your drunk points at 434470 if you can't cheat death
		if var["drunk_points"] >= 434470:
			var["total_drunk_points"] -= (var["drunk_points"] - 434470)
			var["drunk_points"] = 434470

# See stats and your progress	
def profile(var):
	ach = load_file(">")
	dlp = load_file('#')
	drp = load_file('@')
	
	# Fabulous shit
	print("\033[1;35m~~~~~~~~~~~~~~~~~~~~~~~~~") 
	print("|	 PROFILE 	|")
	print("~~~~~~~~~~~~~~~~~~~~~~~~~\033[1;m")
		
	# Amount of beer and drunk points
	print("\033[1;32mDrinks had (this round): " + str(var["drinks_had"]))
	print("Drinks had (all time): " + str(var["total_drinks_had"]))
	print("Drunk points: " + str(round(var["drunk_points"], 2)))
	print("Total drunk points: " + str(round(var["total_drunk_points"], 2)))
	print("High Score: " + str(var["high_score"]) + "\033[1;m")
	
	# Drink Rank
	var["drink_rank"] = calc_drink_rank(var)
	print("\n\033[1;33mDrink Rank: " + drp[var["drink_rank"] - 1][0] + "\033[1;m")
	print("\033[1;34m" + "  - " + drp[var["drink_rank"] - 1][1] + "\033[1;m")
	print("(" + str(calc_drunk_points(var, False)) + "% Alcohol)")
	print("Multiplier: x" + str(calc_multiplier(var, 1)))
	if "Bag of Peanuts" in var["purchased_upgrades"]:
		print("Peanut Bonus: +" + str(calc_nut_bonus(var, 0)))
	print("Total Alcohol: " + str(calc_drunk_points(var, True)) + "%")
	
	# Drunk levels
	print("\n\033[1;33mCurrent Drunk Level: " + str(var["drunk_level"]) + "\033[1;m")
	print("\033[1;34m" + dlp[var["drunk_level"]][0] + "\033[1;m")
	print("\033[1;34m" + "  - " + dlp[var["drunk_level"]][1] + "\033[1;m")
	next_drunk_level = drunk_level_points[var["drunk_level"]] - var["drunk_points"]
	if next_drunk_level == 1:
		print("To next Drunk Level: 1 drunk point")
	elif var["drunk_level"] == 25:
		print("To next Drunk Level: You're dead, there is nothing after this...?")
	else:
		print("To next Drunk Level: " + str(round(next_drunk_level, 2)) + " drunk points")
	
	# Farting and money
	print("\n\033[1;36mFarting now will give you $" + str(calc_money(var)) + "!\033[1;m")
	if var["money_level"] * (var["money_level"] + 1)/2 - var["total_drunk_points"] == 1:
		print("\033[1;36mYou need 1 more drunk point to get more money!\033[1;m")
	else:
		print("\033[1;36mYou need " + str(round(var["money_level"] * (var["money_level"] + 1)/2 - (var["total_drunk_points"] - var["reset_drunk_points"]), 2)) + " more drunk points to get more money!\033[1;m")
	print("\n\033[1;36mTotal money earned: $" + str(var["total_money"]) +"\nCurrent money: $" + str(var["current_money"]) + "\033[1;m")
	
	# Achievements
	print("\n\033[1;34mAchievements: " + str(len(var["unl_ach"])) + "/" + str(len(ach)) + " (" + str(round(len(var["unl_ach"])/len(ach) * 100)) + "%)\033[1;m")
	
	# Peanuts
	if "Bag of Peanuts" in var["purchased_upgrades"]:
		print("Peanuts: " + str(len(var["unl_ach"])))
		print("Flavour: " + fla[var["nut_level"]][0] + " (" + fla[var["nut_level"]][1] + ")")
	
	# Upgrades
	print("\n\033[1;34mUpgrades: " + str(len(var["purchased_upgrades"])) + "/" + str(var["upg_num"]) + " (" + str(round(len(var["purchased_upgrades"])/var["upg_num"] * 100)) + "%)\033[1;m")
		
	# General Counters
	print("\nFarts: " + str(var["fart_count"]))
	print("Deaths: " + str(var["death_count"]))
	print("Lottery Wins: " + str(var["lottery_wins"]))
	
	# Nationality
	print("\nCurrent Nationality: " + nat[var["current_nat"]][0])
	print("Trait: " + nat[var["current_nat"]][1])
	print("Effect: " + nat[var["current_nat"]][2])
	
	print("\n\033[1;35m~~~~~~~~~~~~~~~~~~~~~~~~~\033[1;m\n")

# Checks if you want to and can fart, how much money it gives, and updates High Score
def fart(var):
	if var["drunk_points"] > 0:
		calc_money(var)
		print("\033[1;36mFarting now will give you $" + str(calc_money(var)) + "!\033[1;m")
		print('If you want to fart just press "Enter"!')
		print("If you don't want to fart type something before you press " + '"Enter!"')
		if input("> ") == "":
			fart_reset(var)
		else:
			print("Decided not to fart.")
			# Unlock Achievement "Fart Control"
			unlock_ach(7, var)
	else:
		print("You need to get drunk first!")
		
def fart_reset(var):
	print("\033[1;36mFarted and got $" + str(calc_money(var)) + "!\033[1;m")
	if var["drunk_level"] == 25:
		print("\033[1;36mBy the power of farts you were brought back to life!\033[1;m")
	var["current_money"] += calc_money(var)
	var["total_money"] += calc_money(var)
	if var["high_score"] < var["drunk_points"]:
		var["high_score"] = var["drunk_points"]
	var["drinks_had"] = 0
	var["drunk_points"] = 0
	var["drunk_level"] = 0
	var["new_money"] = 0
	var["available_upgrades"] = []
	var["fart_count"] += 1
	var["switched_nat"] = False
	check_ach(var, "fart")
	
def achievements(var):
	ach = load_file(">")
	ach_list = []
	for n in range(len(ach)):
		if ach[n][0] in var["unl_ach"]:
			ach_list.append(ach[n][0])
		else:
			ach_list.append("???")
	print("\n\033[1;34mAchievements: " + str(len(var["unl_ach"])) + "/" + str(len(ach)) + " (" + str(round(len(var["unl_ach"])/len(ach) * 100)) + "%)\033[1;m\n")
	print('\033[1;34m"' + '", "'.join(ach_list) + '"\033[1;m')
	return

def upgrades(var):
	print("\n\033[1;34mUpgrades: " + str(len(var["purchased_upgrades"])) + "/" + str(var["upg_num"]) + " (" + str(round(len(var["purchased_upgrades"])/var["upg_num"] * 100)) + "%)\033[1;m\n")
	if len(var["purchased_upgrades"]) > 0:
		print('\033[1;34m"' + '", "'.join(var["purchased_upgrades"]) + '"\033[1;m')
	else:
		print('\033[1;34mNo upgrades purchased!\033[1;m')
# Calculates how many drunk points your next drink will give you
def calc_drunk_points(var, total):
	new_drunk_points = calc_drink(0, var["alcohol_level"], calc_base(var, 0.1), var)
	new_drunk_points += calc_drink(0, var["pilsner_level"], calc_base(var, 0.2), var)
	new_drunk_points += calc_drink(0, var["cider_level"], calc_base(var, 0.3), var)
	new_drunk_points += calc_drink(0, var["wine_level"], calc_base(var, 0.5), var)
	new_drunk_points += calc_drink(0, var["liqueur_level"], calc_base(var, 0.7), var)
	new_drunk_points += calc_drink(0, var["martini_level"], calc_base(var, 1), var)
	if total == True:
		if "Bag of Peanuts" in var["purchased_upgrades"]:
			new_drunk_points += calc_nut_bonus(var, 0)
		new_drunk_points *= calc_multiplier(var, 1)
	return round(new_drunk_points, 2)

def calc_base(var, base):
	return base

def calc_drink(alcohol, level, base, var):
	for n in range(level):
		alcohol += base
	return round(alcohol, 1)
	
def calc_multiplier(var, multiplier):
	for n in range(var["mult_level"]):
		multiplier *= ((100 + (n + 1)) / 100)
	if var["current_nat"] == 1:
		multiplier *= 1.5
	if var["prize_duration"] >= 1 and var["current_prize"] == "Super Drink":
		if var["prize_duration"] == 1:
			print("\033[1;35mSuper Drink wore off!\033[1;m")
		else:
			multiplier *= 10
	return round(multiplier, 2)
	
def calc_nut_bonus(var, bonus):
	bonus =  len(var["unl_ach"])/10 * (var["nut_level"] + 1)
	return bonus
	
# Reads bba_text.txt 
def load_file(character):
	ind = 0
	d = {}

	fd = open("bba_text.txt", 'r')
	lines = fd.readlines()
	fd.close()

	for line in (lines):
		if line.startswith(character):
			line = line[2:]
			ind = int(line.split(' {} ')[1][7:])
			line = line.split(' {} ')[0]
			key, value = line.split(' [] ')
			d[ind] = (key, value[:-1])
	
	return d

# Calculates the amount of money your next fart will give	   
def calc_money(var):
	fart_multiplier = 1
	while (var["total_drunk_points"] - var["reset_drunk_points"]) >= var["money_level"] * (var["money_level"] + 1)/2:
		var["new_money"] += 10
		var["money_level"] += 1
	if var["current_nat"] == 4:
		fart_multiplier *= 4
	fart_multiplier *= var["drunk_level"]
	return var["new_money"] * fart_multiplier

# Increases Drunk Level wh +en reaching certain numbers		
def drunk_level_up(var):
	while var["drunk_points"] >= drunk_level_points[var["drunk_level"]]:
		print("\033[1;33mYour Drunk Level went up!\033[1;m")
		var["drunk_level"] += 1
	when_you_die(var)
	return var["drunk_level"]
	
def calc_drink_rank(var):
	while calc_drunk_points(var, False) >= drink_rank_points[var["drink_rank"]]:
		var["drink_rank"] += 1
	return var["drink_rank"]
	
# When you DIE	
def when_you_die(var):
	if var["drunk_level"] == 25:
		print("\n\033[1;31mYOU DIED!!!\033[1;m")
		time.sleep(T)
		print("\nLife Insurance kicked in!")
		time.sleep(T)
		if var["current_nat"] == 0:
			print("\033[1;36mYou got $500000! Hurray!\033[1;m\n")
			var["current_money"] += 500000
			var["total_money"] += 500000
		else:
			print("\033[1;36mYou got $100000! Hurray!\033[1;m\n")
			var["current_money"] += 100000
			var["total_money"] += 100000
		time.sleep(T)
		var["death_count"] += 1

# Everything Lottery related	
def lottery(var):
	odds = 20000
	if var["current_nat"] == 5:
		odds /= 5
	# If a prize is active
	if var["prize_duration"] > 0:
		var["prize_duration"] -= 1
		return var["prize_duration"], var["current_prize"]
	else:
		prize = random.randint(1, odds)
	# Free Drink (+10% Drunk Points)
	if prize > 0 and prize <= 40:
		print("\033[1;35mYou won the Lottery!!!\nPrize: Free Drink")
		print("+ " + str(round(var["drunk_points"] * 0.10)) + " drunk points\033[1;m")
		var["total_drunk_points"] += round(var["drunk_points"] * 0.10)
		var["drunk_points"] = round(var["drunk_points"] * 1.10)
		var["lottery_wins"] += 1
		time.sleep(T)
		return 1, "Free Drink"
	# Instant Interest (+20% Current Money)
	elif prize > 40 and prize <= 80:
		print("\033[1;35mYou won the Lottery!!!\nPrize: Instant Interest")
		print("+ $" + str(round(var["current_money"] * 0.20)) + "\033[1;m")
		var["total_money"] += round(var["current_money"] * 0.20)
		var["current_money"] = round(var["current_money"] * 1.20)
		var["lottery_wins"] += 1
		time.sleep(T)
		return 1, "Instant Interest"
	# Super Drink (Your next 50 drinks are 10 times stronger)
	elif prize > 80 and prize <= 100:
		print("\033[1;35mYou won the Lottery!!!\nPrize: Super Drink")
		print("x10 Alcohol% for the next 50 drinks\033[1;m")
		var["lottery_wins"] += 1
		time.sleep(T)
		return 51, "Super Drink"
	# Increasing Income (+50% Current Money) (Requires British natonality)
	elif prize > 100 and prize <= 120 and var["current_nat"] == 2:
		print("\033[1;35mYou won the Lottery!!!\nPrize: Increasing Income")
		print("+ $" + str(round(var["current_money"] * 0.50)) + "\033[1;m")
		var["total_money"] += round(var["current_money"] * 0.50)
		var["current_money"] = round(var["current_money"] * 1.50)
		var["lottery_wins"] += 1
		time.sleep(T)
		return 1, "Increasing Income"
	# None (You didn't win, better luck next time)
	else:
		return 0, "None"

def can_gamble(var):
	if "Lottery Ticket" not in var["purchased_upgrades"] or var["drunk_level"] >= 25:
		return False
	else:
		return True
		
# Calculate discount
def calc_discount(var):
	discount = 1
	if var["current_nat"] == 3:
		discount -= 0.2
	return discount

# Allows you to buy upgrades to make your adventure more exciting
def shop(var):
	in_shop = True
	print("What do you want to purchase? Type the number next to the alternative to choose.")
	print("[0] Drinks")
	print("[1] Upgrades")
	print("[2] Passports")
	print("[3] Quit")
	action = input("> ")
	if action == "0":
		shop0(var, in_shop)
	elif action == "1":
		shop1(var, in_shop)
	elif action == "2":
		shop2(var)
	elif action == "3":
		return
	else:
		print("Not an available shop!")
		shop(var)

def shop0(var, in_shop):
	print("Type the word in green to buy something (ignore spaces and capitalized letters)")
	print("To buy 10 of something, type 10 after the word.")
	while in_shop:
		i = load_items(var)
		discount = calc_discount(var)
		print('When you want to leave press "Enter".')
		print("\033[1;36mYou have $" + str(var["current_money"]) + "\033[1;m")
		time.sleep(T)
		# Base Alcohol
		print_drink("alcohol", 0.1, var, i, discount)
		# Pilsner
		if var["drunk_level"] >= 5 or var["pilsner_level"] > 0:
			print_drink("pilsner", 0.2, var, i, discount)
		# Cider
		if var["drunk_level"] >= 10 or var["cider_level"] > 0:
			print_drink("cider", 0.3, var, i, discount)
		# Wine
		if var["drunk_level"] >= 15 or var["wine_level"] > 0:
			print_drink("wine", 0.5, var, i, discount)
		# Liqueur
		if var["drunk_level"] >= 20 or var["liqueur_level"] > 0:
			print_drink("liqueur", 0.7, var, i, discount)
		# Martini
		if var["drunk_level"] >= 25 or var["martini_level"] > 0:
			print_drink("martini", 1, var, i, discount)
		# Purchase
		item = input("\nBuy what? ").lower()
		if item == "":
			var["available_upgrades"] = []
			in_shop = False
		elif item not in var["available_upgrades"] and item != "":
			print("You can't buy that!")
		else:
			purchase(var, i, discount, i[item.strip("10")][5], item)

def print_drink(drink, base, var, i, discount):
		var["available_upgrades"].append(drink)
		var["available_upgrades"].append(drink + "10")
		print("\nUpgrade \033[1;32m" + i[drink][0] + "\033[1;m to level " + str(i[drink][3] + i[drink][4])\
		 + "\033[1;36m (x1: $" + str(round(i[drink][1] * discount)) + ") (x10: $" + \
		str(round(round(i[drink][1]/1.10**var[drink + "_level"]) * (1.10**(var[drink + "_level"] + 10) - 1.10**var[drink + "_level"])/0.10 * discount)) + ")\033[1;m")
		print(str(i[drink][2]) + " (Current: " + str(calc_drink(0, var[drink + "_level"], calc_base(var, base), var)) + "% Alcohol)")

def shop1(var, in_shop):
	print("Type the word in green to buy something (ignore spaces and capitalized letters)")
	while in_shop:
		i = load_items(var)
		discount = calc_discount(var)
		print('When you want to leave press "Enter".')
		print("\033[1;36mYou have $" + str(var["current_money"]) + "\033[1;m")
		time.sleep(T)
		# Lottery Ticket
		if var["fart_count"] >= 1 and var["total_drunk_points"] >= 500 and "Lottery Ticket" not in var["purchased_upgrades"]:
			var["available_upgrades"].append("lotteryticket")
			print("\nPurchase \033[1;32m" + i["lotteryticket"][0] + ("\033[1;m \033[1;36m($" + str(i["lotteryticket"][1]) + ")\033[1;m"))
			print(i["lotteryticket"][2])
		# Decoration
		if var["total_drunk_points"] >= (var["mult_level"] + 1)**2 * 10000 and var["mult_level"] < 6:
			var["available_upgrades"].append("decoration")
			print("\nPurchase \033[1;32mDecoration\033[1;m (" + i["decoration"][0] +") (\033[1;m\033[1;36m$" + str(round(i["decoration"][1] * discount)) + ")\033[1;m")
			print(i["decoration"][2])
		# Bag of Peanuts
		if len(var["unl_ach"]) >= 10 and "Bag of Peanuts" not in var["purchased_upgrades"]:
			var["available_upgrades"].append("bagofpeanuts")
			print("\nPurchase \033[1;32mBag of Peanuts\033[1;m (\033[1;m\033[1;36m$" + str(round(i["bagofpeanuts"][1] * discount)) + ")\033[1;m")
			print(i["bagofpeanuts"][2])
		# Peanut+
		if len(var["unl_ach"]) >= 10 * (var["nut_level"] + 2) and "Bag of Peanuts" in var["purchased_upgrades"] and var["nut_level"] < 2:
			var["available_upgrades"].append("peanut+")
			print("\nPurchase \033[1;32mPeanut+\033[1;m (\033[1;m\033[1;36m$" + str(round(i["peanut+"][1] * discount)) + ")\033[1;m")
			print(i["peanut+"][2])
		# Contract
		if var["drunk_level"] >= 25:
			var["available_upgrades"].append("contract")
			print("\nPurchase \033[1;32mContract\033[1;m \033[1;36m($" + str(round(500000 * discount)) + ")\033[1;m")
			print("Makes it easier to get money")
		# Purchase
		if len(var["available_upgrades"]) == 0:
			print("You can't buy anything!")
			in_shop = False
		else:
			item = input("\nBuy what? ").lower()
			if item == "":
				var["available_upgrades"] = []
				in_shop = False
			elif item not in var["available_upgrades"] and item != "":
				print("You can't buy that!")
			elif item == "contract" and item in var["available_upgrades"]:
				buy_contract(var, discount)
			else:
				purchase(var, i, discount, i[item][5], item)
				
def shop2(var):
	if var["switched_nat"] == False:
		in_shop = True
		available_nat = []
		print("Here you can change your nationality.\nYou unlock a nationality for every Level 50 drink you have.")
		print("All nationalities have different effects.\nSwitching nationality costs $5000 and you can only switch once per round.")
		print("Current Nationality: " + nat[var["current_nat"]][0])
		while in_shop == True:
			print("\033[1;36mYou have $" + str(var["current_money"]) + "\033[1;m")
			print("Press Enter to quit.")
			time.sleep(T)
			# Print available nationalities
			if var["current_nat"] != 0:
				available_nat.append("swedish")
				print_nat(0)
			if var["pilsner_level"] >= 50 and var["current_nat"] != 1:
				available_nat.append("german")
				print_nat(1)
			if var["cider_level"] >= 50 and var["current_nat"] != 2:
				available_nat.append("british")
				print_nat(2)
			if var["wine_level"] >= 50 and var["current_nat"] != 3:
				available_nat.append("italian")
				print_nat(3)
			if var["liqueur_level"] >= 50 and var["current_nat"] != 4:
				available_nat.append("spanish")
				print_nat(4)
			if var["martini_level"] >= 50 and var["current_nat"] != 5:
				available_nat.append("american")
				print_nat(5)
			# Switching nationality
			new_nat = input("\nWhich nationality do you want to switch to?\n> ").lower()
			if new_nat == "swedish" and new_nat in available_nat:
				var["current_nat"], in_shop = switch_nat(var, 0, in_shop)
			elif new_nat == "german" and new_nat in available_nat:
				var["current_nat"], in_shop = switch_nat(var, 1, in_shop)
			elif new_nat == "british" and new_nat in available_nat:
				var["current_nat"], in_shop = switch_nat(var, 2, in_shop)
			elif new_nat == "italian" and new_nat in available_nat:
				var["current_nat"], in_shop = switch_nat(var, 3, in_shop)
			elif new_nat == "spanish" and new_nat in available_nat:
				var["current_nat"], in_shop = switch_nat(var, 4, in_shop)
			elif new_nat == "american" and new_nat in available_nat:
				var["current_nat"], in_shop = switch_nat(var, 5, in_shop)
			elif new_nat == "":
				in_shop = False
			else:
				print("That's not an available nationality!")
	else:
		print("You've already switched nationality.\nCome back after farting.")

def print_nat(nation):
	print("\nNationality: " + nat[nation][0])
	print("Trait: " + nat[nation][1])
	print("Effect: " + nat[nation][2])
		
def switch_nat(var, nation, in_shop):
	if var["current_money"] >= 5000:
		var["current_money"] -= 5000
		var["current_nat"] = nation
		print("You're now " + nat[var["current_nat"]][0])
		var["switched_nat"] = True
		in_shop = False
		return var["current_nat"], in_shop
	else:
		print("Not enough money...")
		return var["current_nat"], in_shop

def purchase(var, i, discount, item_type, item):
	# Level up a drink 10 times
	if item_type == "drink" and item.endswith("10"):
		item = item[:-2]
		i[item][3] = buy_item((round(round(i[item][1]/1.10**var[item + "_level"]) * (1.10**(var[item + "_level"] + 10) - 1.10**var[item + "_level"])/0.10 * discount))\
		, i[item][3], 10, var, item_type, i[item][0])
	else:
		i[item][3] = buy_item(round(i[item][1] * discount), i[item][3], i[item][4], var, item_type, i[item][0])	  
	if item_type == "drink":
		var[item + '_level'] = i[item][3]
	elif item_type == "nutlvlup":
		var["nut_level"] = i[item][3]
	elif item == "decoration":
		var["mult_level"] = i[item][3]
	# Reset upgrades
	var["available_upgrades"] = []
	check_ach(var, "shop")
		
def load_items(var):
	# Item name, Base Price, Description, Variable, Modifier
	items = {"alcohol": ["Alcohol", round(20 * 1.10**var["alcohol_level"]), "+" + str(calc_base(var, 0.1)) + "% Alcohol", var["alcohol_level"], 1, "drink"],
			 "pilsner": ["Pilsner", round(45 * 1.10**var["pilsner_level"]), "+" + str(calc_base(var, 0.2)) + "% Alcohol", var["pilsner_level"], 1, "drink"],
			 "cider": ["Cider", round(75 * 1.10**var["cider_level"]), "+" + str(calc_base(var, 0.3)) + "% Alcohol", var["cider_level"], 1, "drink"],
			 "wine": ["Wine", round(138 * 1.10**var["wine_level"]), "+" + str(calc_base(var, 0.5)) + "% Alcohol", var["wine_level"], 1, "drink"],
			 "liqueur": ["Liqueur", round(210 * 1.10**var["liqueur_level"]), "+" + str(calc_base(var, 0.7)) + "% Alcohol", var["liqueur_level"], 1, "drink"],
			 "martini": ["Martini", round(325 * 1.10**var["martini_level"]), "+" + str(calc_base(var, 1)) + "% Alcohol", var["martini_level"], 1, "drink"],
			 "lotteryticket": ["Lottery Ticket", 1, "Unlocks the Lottery", "lotteryticket", "@", "upgrade"],
			 "decoration": [dec[var["mult_level"]], (var["mult_level"] + 1) * 10**var["mult_level"], "Multiplier +" + str(var["mult_level"] + 1) + "%", var["mult_level"], 1, "upgrade"],
			 "bagofpeanuts": ["Bag of Peanuts", 5, "Unlocks peanuts, which give drunk points. You get peanuts from achievements.", "bagofpeanuts", "@", "upgrade"],
			 "peanut+": ["Peanut+", 12**(var["nut_level"] + 2), "Peanuts give more drunk points.", var["nut_level"], 1, "nutlvlup"]
			}
	return items

# Checks what you're buying
def buy_item(cost, item, modifier, var, item_type, item_name):
	if can_buy(cost, var) == True:
		item += modifier
		if item_type == "upgrade":
			var["purchased_upgrades"].append(item_name)
	else:
		print("\nNot enough money...")
	return item

# Checks if you have enough money to buy it	
def can_buy(cost, var):
	if var["current_money"] >= cost:
		var["current_money"] -= cost
		return True
	else:
		return False

def buy_contract(var, discount):
	if can_buy(500000 * discount, var) == True:
		var["reset_drunk_points"] += var["total_drunk_points"] - var["reset_drunk_points"]
		var["money_level"] = 1
		print("\n\033[1;36mGetting money is easier now\033[1;m")
	else:
		print("\nNot enough money...")
	return var

# Checks if you have unlocked an achievement
def check_ach(var, status):
	ach = load_file(">")
	# Unlock Achievement "One Drink to rule them all"
	if var["total_drinks_had"] >= 1:
		unlock_ach(2, var)
	# Unlock Achievement "Literally a drinking game"
	if var["total_drinks_had"] >= 10000:
		unlock_ach(3, var)
	# Unlock Achievement "Beerly Started"
	if var["drunk_points"] >= 100:
		unlock_ach(4, var)
	# Unlock Achievement "Drunk. Drunk everywhere."
	if var["drunk_points"] >= 100000:
		unlock_ach(5, var)
	# Unlock Achievement "Release the pressure"
	if var["fart_count"] >= 1:
		unlock_ach(6, var)
	# Unlock Achievement "Killed by a beer"
	if var["death_count"] >= 1:
		unlock_ach(8, var)
	# Unlock Achievement "I wouldn't drink that if I were you"
	if calc_drink_rank(var) >= 6:
		unlock_ach(9, var)
	# Unlock Achievement "Outlived the cat"
	if var["death_count"] >= 10:
		unlock_ach(11, var)
	# Unlock Achievement "Battle the odds"
	if var["lottery_wins"] >= 1:
		unlock_ach(12, var)
	# Unlock Achievement "Luck-ception"
	if var["lottery_wins"] >= 7:
		unlock_ach(13, var)
	# Unlock Achievement "In the 0.00000001%"
	if var["lottery_wins"] >= 77:
		unlock_ach(14, var)
	# Unlock Achievement "Gas Leak"
	if var["fart_count"] >= 10:
		unlock_ach(15, var)
	# Unlock Achievement "First Buy"
	if len(var["purchased_upgrades"]) >= 1:
		unlock_ach(16, var)
	# Unlock Achievement "Shopalcoholic"
	if len(var["purchased_upgrades"]) == var["upg_num"]:
		unlock_ach(17, var)
	# Unlock Achievement "Going nuts"
	if "Bag of Peanuts" in var["purchased_upgrades"]:
		unlock_ach(18, var)
	# Unlock Achievement "What even?"
	if calc_drink_rank(var) >= 8:
		unlock_ach(19, var)
	# Unlock Achievement "Alcomore"
	if var["alcohol_level"] >= 50:
		unlock_ach(20, var)
	# Unlock Achievement "Oktoberfest!"
	if var["pilsner_level"] >= 50:
		unlock_ach(21, var)
	# Unlock Achievement "Yes, quite"
	if var["cider_level"] >= 50:
		unlock_ach(22, var)
	# Unlock Achievement "Nothing to Wine about"
	if var["wine_level"] >= 50:
		unlock_ach(23, var)
	# Unlock Achievement "Liqueur, Sliqueur"
	if var["liqueur_level"] >= 50:
		unlock_ach(24, var)
	# Unlock Achievement "MURICAH!!!"
	if var["martini_level"] >= 50:
		unlock_ach(25, var)
	# Unlock Achievement "Well that was quick..."
	if var["drunk_level"] >= 25 and var["drinks_had"] <= 500:
		unlock_ach(26, var)
	# Unlock Achievement "KACHING!"
	if var["current_money"] >= 100000:
		unlock_ach(27, var)
	# Unlock Achievement "The Drunk Millionare"
	if var["current_money"] >= 1000000:
		unlock_ach(28, var)
	# Unlock Achievement "Stumped the Trump"
	if var["current_money"] >= 100000000:
		unlock_ach(29, var)
		
# Unlocks achievements
def unlock_ach(achievement, var):
	ach = load_file(">")
	if ach[achievement][0] not in var["unl_ach"]:
		print("\n\033[1;34mAchievement Unlocked: " + ach[achievement][0])
		print("[" + ach[achievement][1] + "]\033[1;m\n")
		var["unl_ach"].append(ach[achievement][0])
		time.sleep(T)

def main():
	
	# Variables
	var = {'high_score': 0,
		   'drinks_had': 0,
		   'total_drinks_had': 0,
		   'drunk_points': 0,
		   'total_drunk_points': 0,
		   'reset_drunk_points': 0,
		   'drunk_level': 0,
		   'drink_rank': 0,
		   'total_money': 20,
		   'current_money': 20,
		   'new_money': 0,
		   'money_level': 1,
		   'alcohol_level': 0,
		   'ethanol_level': 0,
		   'pilsner_level': 0,
		   'cider_level': 0,
		   'wine_level': 0,
		   'liqueur_level': 0,
		   'martini_level': 0,
		   'mult_level': 0,
		   'nut_level': 0,
		   'fart_count': 0,
		   'death_count': 0,
		   'lottery_wins': 0,
		   'prize_duration': 0,
		   'current_prize': "None",
		   'current_nat': 0,
		   'switched_nat': False,
		   'available_upgrades': [],
		   'purchased_upgrades': [],
		   'unl_ach': [],
		   'upg_num': 8
		   }
	
	# Play game
	story(var)
	choose_action(var)


main()
