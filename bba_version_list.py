# --------------
# Indev Releases
# --------------

# Indev 0.01
# ----------
# Added story
# Added "drink" command
# Added "help" comand
# Added "profile" command

# Indev 0.02
# ----------
# Drunk levels work properly
# "To next Drunk Level" stat works properly
# Changed story speed

# Indev 0.021
# -----------
# Drunk levels now have names (Up to Level 10)
# Added comments in the code

# Indev 0.022
# -----------
# Added sub-text for every Drunk Level (Up to Level 10)

# Indev 0.03
# ----------
# Cleaned up the code
# Added "fart" command
# Added money
# Added "shop" command
# Added Alcohol + Upgrade
# Changed profile formatting
# Added Pilsner Upgrade

# Indev 0.04
# ----------
# You only need to press "Enter" to drink
# Added Wine upgrade
# Sub-text and Drunk Level names now go up to Drunk Level 15
# Added Extra Drunk upgrade
# Alcohol+, Pilsner and Wine now caps at level 5
# Added Ice Cube upgrade
# Added Brain Freeze upgrade
# Changed the code that determins how many drunk points you get

# Indev 0.041
# -----------
# Changed the color of some text
# Fixed a typo
# Now shows how many Drunk Points you get per beer in profile

# Indev 0.042
# -----------
# Added more info in profile and shop
# Removed a line of code that didn't do anything
# Added item descriptions in the shop

# Indev 0.05
# ----------
# Wine and Pilsner is harder to unlock
# Wine is better but more expensive
# Added Cocktail upgrade
# Added Whisky upgrade
# Changed how the shop works, A LOT
# Sub-text and Drunk Level names now go up to level 20
# Added Ethanol upgrade
# Added Extra Drunk + upgrade

# Indev 0.051
# -----------
# Fixed Ethanol being spelled "Etanol"
# Tells you how much money you get for farting before farting and in profile
# You need to confirm before farting
# Tweaked the shop code
# Fart Counter... because reasons

# --------------
# Alpha Releases
# --------------

# Alpha 0.06 (Released January 2nd 2016)
# Main Feature: You can officially beat the game
# ----------------------------------------------
# You can now "beat" the game by reaching Drunk Level 25 (MAX) and dying (farting will bring you back to life)
# Updated Drunk Level text
# Updated Drunk Level formula
# Updated Money formula
# Added Life Insurance, every time you die you get $100000000
# Money is now refered to as $ in some places
# Ethanol is now refered to as Pure Ethanol in some places
# Added Total Drunk points stat
# Added High Score but it doesn't do anything currently
# Rebalanced the cost and effect of upgrades A LOT
# Profile is more fabulous
# Changed and added minor text

# Alpha 0.07 (Released January 28th 2016)
# Main Feature: New Upgrade System
# --------------------------------
# Dictionaries and R.I.P Global Variables
# Fixed some stuff and changed some text
# Added bba_text.txt to store text
# Removed Pure Ethanol (for now) because it was OP
# Added Drink Rank
# You now have to buy Alcohol+ once before being able to drink
# You now start the game with some money
# Added Death Counter
# High Score now tracks your high score
# Nerfed Life Insurance to $100000
# Removed the Alcohol+ cap
# Redesigned the entire upgrade system
# Removed/Added upgrades
# Rebalanced prices (again)
# Added 5 upgrade tiers

# Alpha 0.08 (Released February 10th 2016)
# Main Feature: More Interesting Gameplay
# ---------------------------------------
# Some minor changes
# Added a new Drink Rank
# Ethanol+ has returned, now with a new effect
# Added Lottery Ticket upgrade (Unlocks Lottery)
# Added Lottery with 3 default random prizes that boost various stats
# Nerfed all drinks slightly
# Added "passport" command
# Added 6 Nationalities that all have unique boosts
# Version Log now more detailed for Alpha (and future) releases
# Now displys Alcohol Level in shop

# Alpha 0.081 (Released February 11th 2016)
# Main Feature: Patch
# -----------------------------------------
# Fixed some crud

# Alpha 0.082 (Released February 24th 2016)
# Main Feature: Achievements
# ---------------------------
# I can't spell british
# Updated the intro text and some other things
# Added Drinks had (all time) stat
# Achievements!!! (10 for now, will add more in the future)
# Added 6 decorations (increases your multiplier)
# Fixed Ethanol Price

# Alpha 0.09 (Released May 25th 2016)
# Main Feature: New Shop System
# ---------------------------
# Completely remodeled the shop
# Changed how drinks work
# You now change nationalities via the shop
# Easy Money is now called Contract
# Added "upgrades" command
# Added Peanuts (3 flavours for now)
# Added 20 more achievements
